import face_recognition
import os
import math
from PIL import Image, ImageDraw


def get_frame_from_face(top, bottom, left, right, image):
    """ The real magic here is in centering the face in every image
        I thought about how to do this using my big brain energy and
        a whiteboard and it made me very happy
    """

    # get the overall size of the image
    pil_image = Image.fromarray(image)
    overall_x = pil_image.size[0]
    overall_y = pil_image.size[1]

    # get the size of the face in pixels
    y_dim = bottom - top
    x_dim = right - left

    # get dimensions of a 1000 x 1000 pixel image centered on my face
    new_top = max(0, top - math.ceil((1000 - y_dim) / 2))
    new_bottom = min(overall_y, bottom + math.floor((1000 - y_dim) / 2))
    new_left = max(0, left - math.ceil((1000 - x_dim) / 2))
    new_right = min(overall_x, right + math.floor((1000 - x_dim) / 2))

    # print("({}, {})\n({}, {})".format(new_left, new_top, new_right, new_bottom))

    # actually get the face image
    face_image = image[new_top:new_bottom, new_left:new_right]
    new_size = (1000, 1000)
    pil_image = Image.fromarray(face_image)

    # create new black PIL image, then paste my face at the center of it
    new_im = Image.new("RGB", (1000, 1000))
    new_im.paste(pil_image, (math.ceil((new_size[0] - pil_image.size[0]) / 2),
                             (math.ceil((new_size[1] - pil_image.size[1]) / 2))))

    return new_im


def get_all_file_names(path="sorted_photos"):
    all_files = []
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            if name.endswith((".jpg", ".jpeg")):
                path = os.path.join(root, name)
                all_files.append(path)

    return all_files


def save_all_frames_to_files(all_files, start_frame, ethan_face_encoding, max_length=20):
    """ The magic here is in updating the algorithm's encoding of my face over time.
        This is done by continually updating the encoding with the oldest picture.
        Some of this is automatically done (when the face found is very similar to
        the previous encoding), and some is done manually (by asking the user to select
        a good encoding of the face)
    """
    
    length_counter = 0
    frame_counter = start_frame

    for path in reversed(all_files):
        # load a picture we haven't seen before, find all the faces, and use the face encodings for recognition
        unknown_picture = face_recognition.load_image_file(path)
        face_locations = face_recognition.face_locations(unknown_picture)
        face_encodings = face_recognition.face_encodings(
            unknown_picture, face_locations)
        print("Attempting to find match in {}".format(path))

        # for each face, get the location of the face and use the face encoding to see if it matches the "ethan face"
        for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):

            # See if the face is a match for the known face(s)
            face_distances = face_recognition.face_distance(
                [ethan_face_encoding], face_encoding)

            # used for exporting
            frame_name = "frame{}.jpg".format(frame_counter)

            # if the distance between faces is small, face matches my face (see face_recognition docs for more info)
            if face_distances[0] < .4:
                # print that the face was good, then export to file
                print("Found an AWESOME match! {} exporting to {}".format(
                    face_distances[0], frame_name))
                frame = get_frame_from_face(
                    top, bottom, left, right, unknown_picture)
                frame.save("exported/{}".format(frame_name))

                ethan_face_encoding = face_encoding  # update the running face encoding
                frame_counter += 1  # increment the number of frames
                length_counter = 0  # reset the time between good faces
                break
            # if it's a good match and it's been a while since we had an awesome match, make sure it's the right person
            elif face_distances[0] < .5 and length_counter > max_length:
                print("We're trying to get a new encoding from {}".format(frame_name))
                frame = get_frame_from_face(
                    top, bottom, left, right, unknown_picture)
                frame.show()
                frame.save("exported/{}".format(frame_name))
                yes_no = str(
                    input("Is this a good picture of your face? (y/n): ")) == "y"
                if yes_no:
                    ethan_face_encoding = face_encoding
                    length_counter = 0
                frame_counter += 1
                break
            # if it's a good match but it hasn't been too long since an awesome or confirmed match, just save it
            elif face_distances[0] < .5:
                print("Found a good match ({}) exporting to {}".format(
                    face_distances[0], frame_name))
                frame = get_frame_from_face(
                    top, bottom, left, right, unknown_picture)
                frame.save("exported/{}".format(frame_name))
                frame_counter += 1
                break
            # if it's not a great match, but it might be a match, save it to a different directory for later manual review
            elif face_distances[0] < .6:
                print("Found a questionable match ({}) exporting to maybes/{}".format(
                    face_distances[0], frame_name))
                frame = get_frame_from_face(
                    top, bottom, left, right, unknown_picture)
                frame.save("maybes/{}".format(frame_name))
                frame_counter += 1
                length_counter += 1
            else:
                print("This face not a match in {}".format(path))

            print("\n")


def main():
    import argparse

    parser = argparse.ArgumentParser(
        description="A tool to find a certain person's face in images and stitch that together into a video")

    parser.add_argument('-i', '--interactive', action='store_true', help='will pause when program needs a new encoding',
                        default=False)

    frame_counter = 0

    all_files = get_all_file_names()

    print(all_files)

    # Load the initial, known jpg file into a numpy array
    known_image = face_recognition.load_image_file("IMG_20200521_122355.jpg")
    # get the face encoding and location
    ethan_face_encoding = face_recognition.face_encodings(known_image)[0]
    ethan_face_location = face_recognition.face_locations(known_image)[0]
    top, bottom, left, right = ethan_face_location
    # get the first frame and export it
    frame = get_frame_from_face(top, bottom, left, right, known_image)
    frame_name = "frame{}.jpg".format(frame_counter)
    frame.save("exported/{}".format(frame_name))
    frame_counter += 1

    save_all_frames_to_files(all_files, frame_counter, ethan_face_encoding)


main()
