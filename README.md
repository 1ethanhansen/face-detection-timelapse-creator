# face-detection-timelapse-creator
Give it a bunch of (sorted) images and a face and get a timelapse of that face

I made this so I could give my mom a graduation present as a thank you for being an incredible mom for the first 18 years of my life. Thanks mom!

## How to use this project

There are 3 overall steps:
1. Sort the photos (not done by this repo)
2. Export frames for a video with the wanted face (done by this repo)
3. Convert the frames to a video (done by this repo)

### Sort the photos

1. Get a directory of photos sorted by date (that have your face in them)
    1. It _might_ work with pictures that don't have your face in them, but that hasn't been tested. I used just pictures that I knew had my face in them

2. The directory should be structured like this with sorted_photos a  subdirectory of the directory holding `main.py` from this project (i know i know it's quick and dirty. if people care I can clean that up so you can specify the path to sorted_photos):
    ```
    sorted_photos
    ├── 2002
    │   ├── 06-Jun
    │   │   ├── photo1.jpg
    │   │   ├── randomphoto.jpg
    │   │   └── ...
    │   ├── 07-Jul
    │   │   ├── photo2.jpg
    │   │   ├── anotherphoto.jpg
    │   │   └── ...
    │   └── 12-Dec
    ├── 2003
    │   ├── 06-Jun
    │   └── 12-Dec
    ...
    ```
    or in general
    ```
    sorted_photos
    ├── <year>
    │   ├── <month_number>-<month_name>
    │   │   ├── *.jpg
    ```
    I'm assuming that within a single month your face won't change too much to have the order of those photos matter. If you want to make it do that, please open up an issue or merge request on GitLab and let's work together on that :)
    
3. My recommendation is to use [sortphotos.py](https://github.com/andrewning/sortphotos) which is what I did. The only catch is that the EXIF data for date created or date modified has to exist for the files or it won't know how to sort them. You can edit that manually with an EXIF editor (i know because I did that for old photos that I scanned in for this project, but unfortunately I forgot which one I used oops).

### Get the frames

1. Get the required package the facial recognition is based on: https://github.com/ageitgey/face_recognition#installation (as a side note this is an INCREDIBLE project and you should show the developers some love with stars)

2. Put a starting jpg file in the root directory of the project, then point to it on line 150 of `main.py` as your first `known_image`. This photo should only have you in it and be a good representation of you (preferably directly looking at the camera)

3. Run `python main.py` with `main.py` in the directory housing the sorted_photos directory

4. Wait a LONG time. It's not the fastest program and you'll have to check in every once in a while to see if there's any "questionable matches" that the program is going to ask you about. These are used to re-train/fine-tune the model if it's gone a while without finding a good enough face to automatically re-train on.

### Create the video

1. Make sure there is a directory called `exported`. This should be populated with {frame0.jpg, frame1.jpg, ... framen.jpg} by the previous step

2. Run `frames_to_video.py`

    1. You might want to adjust the frame rate. You can do this by editing the `frames_per_second` parameter

3. Voila!! A timelapse has been created


## How it works

Essentially, photos are fed into the program. Over time, the program learns and re-learns the encoding of the face of the person trained on. This allows it to remain fairly accurate over an arbitrary number of years of photos. The photos are processed into frames and exported, keeping the face centered. Those frames are finally stiched together into a final video file 

For more info, see the multi-line comments explaining the magic in the `get_frame_from_face` and `save_all_frames_to_files` functions in `main.py`

## Future improvements
Low-ish hanging fruit:
* make interactive mode optional for `main.py`
* make interactive mode have sensitivity parameters
* add argument for path to sorted_photos
* add argument for frame size
* add argument for frame rate
* make frames_to_video auto-detect frame size and max_frame_count
* add an option to frames_to_video to give a desired length of the timelapse (in seconds) and automatically use the correct frame rate
* add argument for path to inital_photo

potentially a bit more work:
* make it so the user doesn't have to go to the exported directory to see the pictures being asked about
* make it so the user can use the maybes without having to manually copypast into exported
* allow for multiple file formats and automatically breaking movies down into individual photos for processing
