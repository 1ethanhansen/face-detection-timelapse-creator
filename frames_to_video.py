import cv2
import os
import argparse

parser = argparse.ArgumentParser(
    description="The portion that turns frames into a video")


def frames_to_video(path="exported", max_frame_count=1000, frames_per_second=10):
    fourcc = cv2.VideoWriter_fourcc(*"MJPG")
    video = cv2.VideoWriter("timelapse.avi", fourcc, frames_per_second, (1000, 1000))

    for frame_number in range(max_frame_count, -1, -1):
        file_name = "{}/frame{}.jpg".format(path, frame_number)
        if os.path.isfile(file_name):
            frame = cv2.imread(file_name)
            video.write(frame)

    cv2.destroyAllWindows()
    video.release()


frames_to_video(max_frame_count=717, frames_per_second=2)
